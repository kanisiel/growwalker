//
//  File.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 7. 4..
//  Copyright © 2016년 Team6. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class QuestService {
    let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    let formatter = NSNumberFormatter()
    
    func initData(plantsService:PlantsService){
        let data = getAll()
        if data.count < 1 {
            let list = QuestList()
            let array = list.getArray(plantsService)
            for no in 0 ..< array.count {
                var clear:NSNumber = 0
                if no == 0 {
                    clear = 2
                }
                let dictionary = array[no]
                let target:NSNumber = formatter.numberFromString(dictionary["target"]! as! String)!
                create(no, title: dictionary["title"]! as! String, body: dictionary["body"]! as! String, prev: formatter.numberFromString(dictionary["prev"]! as! String)!, gold: formatter.numberFromString(dictionary["gold"]! as! String)!, items: dictionary["items"]! as! String, energy: formatter.numberFromString(dictionary["energy"]! as! String)!, clear: clear, target: target, criteria: dictionary["criteria"]! as! String)
            }
        }
    }
    
    func create(no: NSNumber, title: String, body:String, prev:NSNumber, gold:NSNumber, items:String, energy:NSNumber, clear:NSNumber, target:NSNumber, criteria:String) -> Quest {
        
        let newItem = NSEntityDescription.insertNewObjectForEntityForName(Quest.entityName, inManagedObjectContext: context) as! Quest
        
        
        newItem.no = no
        newItem.title = title
        newItem.body = body
        newItem.prev = prev
        newItem.rewardGold = gold
        newItem.rewardItems = items
        newItem.rewardEnergy = energy
        newItem.clear = clear
        newItem.current = 0
        newItem.target = target
        newItem.criteria = criteria
        
        return newItem
    }
    
    func get(no:NSNumber) -> Quest {
        let quests:[Quest] = getAll()
        
        if(quests.count > 0){
            for quest in quests {
                if ( quest.no == no ){
                    return quest
                }
            }
            return Quest()
        } else {
            return Quest()
        }
    }
    
//    func get(no:NSNumber) -> Quest {
//        
//        let predicate = NSPredicate.init(format: "no = %@", no)
//        let fetchRequest = NSFetchRequest(entityName: Quest.entityName)
//        
//        fetchRequest.predicate = predicate
//        // var index = 0;
//        do {
//            let response = try context.executeFetchRequest(fetchRequest)
//            if(response.count < 1){
//                return Quest()
//            } else {
//                return response.last as! Quest
//            }
//        } catch let error as NSError {
//            // failure
//            print(error)
//            return Quest()
//        }
//        
//    }
    // Gets a Status by id
    func getById(id: NSManagedObjectID) -> Quest? {
        return context.objectWithID(id) as? Quest
    }
    
    // Updates a Status
    func update(updatedStatus: Quest){
        if let Quest = getById(updatedStatus.objectID){
            Quest.clear = updatedStatus.clear
        }
    }
    
    func getAll() -> [Quest]{
        return getWith(withPredicate: NSPredicate(value:true))
    }
    
    func getWith(withPredicate queryPredicate: NSPredicate) -> [Quest]{
        let fetchRequest = NSFetchRequest(entityName: Quest.entityName)
        
        fetchRequest.predicate = queryPredicate
        
        do {
            let response = try context.executeFetchRequest(fetchRequest)
            return response as! [Quest]
            
        } catch let error as NSError {
            // failure
            print(error)
            return [Quest]()
        }
    }
    
    
    // Deletes a Status
    func deleteStatus(id: NSManagedObjectID){
        if let statusToDelete = getById(id){
            context.deleteObject(statusToDelete)
        }
    }
}
