//
//  TimeService.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 7. 8..
//  Copyright © 2016년 Team6. All rights reserved.
//

import Foundation

class TimeService {
    
    func makeDate(year:Int, month:Int, day:Int) -> NSDate {
        let calendar = NSCalendar.currentCalendar()
        let dateComponents = NSDateComponents()
        
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        dateComponents.hour = 0
        dateComponents.minute = 0
        dateComponents.second = 0
        dateComponents.nanosecond = 0
        let date = calendar.dateFromComponents(dateComponents)
        return date!
    }
    func makeDateTime(year:Int, month:Int, day:Int, hour:Int, minute:Int, second:Int) -> NSDate {
        let calendar = NSCalendar.currentCalendar()
        let dateComponents = NSDateComponents()
        
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        dateComponents.hour = hour
        dateComponents.minute = minute
        dateComponents.second = second
        dateComponents.nanosecond = 0
        
        let datetime = calendar.dateFromComponents(dateComponents)
        return datetime!
    }
    func makeTime(hour:Int, minute:Int, second:Int) -> NSDate {
        let calendar = NSCalendar.currentCalendar()
        let dateComponents = NSDateComponents()
        
        dateComponents.year = calendar.component(NSCalendarUnit.Year, fromDate: NSDate())
        dateComponents.month = calendar.component(NSCalendarUnit.Month, fromDate: NSDate())
        dateComponents.day = calendar.component(NSCalendarUnit.Day, fromDate: NSDate())
        dateComponents.hour = hour
        dateComponents.minute = minute
        dateComponents.second = second
        dateComponents.nanosecond = 0
        
        let datetime = calendar.dateFromComponents(dateComponents)
        return datetime!
    }
    func makeTimeAfter(hour:Int, minute:Int, second:Int) -> NSDate {
        let calendar = NSCalendar.currentCalendar()
        let date = NSDate()
        
        let addSecond = calendar.dateByAddingUnit(NSCalendarUnit.Second, value: second, toDate: date, options: [])!
        let addMinute = calendar.dateByAddingUnit(NSCalendarUnit.Minute, value: minute, toDate: addSecond, options: [])!
        let addHour = calendar.dateByAddingUnit(NSCalendarUnit.Hour, value: hour, toDate: addMinute, options: [])!
        
        return addHour
    }
    func getTimeDiff(fromDate:NSDate, toDate:NSDate) -> NSDateComponents {
        let calendar:NSCalendar = NSCalendar.currentCalendar()
        
        return calendar.components([.Day, .Hour, .Minute, .Second, .Nanosecond], fromDate: fromDate, toDate: toDate, options: [])
    }
    func getSeconds(comp:NSDateComponents) -> Int {
        var rv:Int = 0
        
        rv = rv + comp.second + (comp.minute * 60) + (comp.hour * 60 * 60) + (comp.day * 60 * 60 * 24 )
        
        return rv
    }
    func getMinute(comp:NSDateComponents) -> Int {
        var rv:Int = 0
        
        rv = rv + comp.second + (comp.minute * 60) + (comp.hour * 60 * 60) + (comp.day * 60 * 60 * 24 )
        
        return rv/60
    }
}