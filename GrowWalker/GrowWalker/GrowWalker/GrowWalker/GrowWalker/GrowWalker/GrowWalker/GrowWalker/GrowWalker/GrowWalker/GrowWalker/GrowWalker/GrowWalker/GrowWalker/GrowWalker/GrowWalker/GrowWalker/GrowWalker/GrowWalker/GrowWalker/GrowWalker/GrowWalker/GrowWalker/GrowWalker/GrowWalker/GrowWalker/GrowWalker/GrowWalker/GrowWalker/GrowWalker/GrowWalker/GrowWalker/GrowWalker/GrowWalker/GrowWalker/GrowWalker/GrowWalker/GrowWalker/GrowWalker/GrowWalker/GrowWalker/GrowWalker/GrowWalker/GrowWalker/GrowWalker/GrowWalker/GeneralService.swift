//
//  GeneralService.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 7. 4..
//  Copyright © 2016년 Team6. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class GeneralService {
    let timeService = TimeService()
    var entityDescription:NSEntityDescription?
    var generalStatus: NSManagedObject?
    let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    // Saves all changes
    func saveChanges(){
        do{
            try context.save()
        } catch let error as NSError {
            // failure
            print(error)
        }
    }
    func create(gold: NSNumber, energy: NSNumber, ground:NSNumber) -> General {
        
        let newItem = NSEntityDescription.insertNewObjectForEntityForName(General.entityName, inManagedObjectContext: context) as! General
        
        newItem.gold = gold
        newItem.energy = energy
        newItem.ground = ground
        newItem.water = 0
        newItem.moisture = 100
        newItem.latestUpdate = NSDate()
        newItem.dryTime = timeService.makeTimeAfter(5, minute: 0, second: 0)
        newItem.cropCount = 0
        self.saveChanges()
        return newItem
    }
    
    // Gets a Status by id
    func getById(id: NSManagedObjectID) -> General? {
        return context.objectWithID(id) as? General
    }
    
    func get() -> General {
        
        let predicate = NSPredicate.init(value: true)
        
        let fetchRequest = NSFetchRequest(entityName: General.entityName)
        
        fetchRequest.predicate = predicate
        
        do {
            let response = try context.executeFetchRequest(fetchRequest)
            if(response.count<1){
                return create(0, energy: 0, ground:1)
            } else {
                return response.last as! General
            }
            
        } catch let error as NSError {
            // failure
            print(error)
            return General()
        }
        
    }
    
    func getAll() -> [General]{
        return getWith(withPredicate: NSPredicate(value:true))
    }
    
    func getWith(withPredicate queryPredicate: NSPredicate) -> [General]{
        let fetchRequest = NSFetchRequest(entityName: General.entityName)
        
        fetchRequest.predicate = queryPredicate
        
        do {
            let response = try context.executeFetchRequest(fetchRequest)
            return response as! [General]
            
        } catch let error as NSError {
            // failure
            print(error)
            return [General]()
        }
    }
    
    // Updates a Status
    func update(updatedStatus: General){
        if let general = getById(updatedStatus.objectID){
            general.gold = updatedStatus.gold
            general.energy = updatedStatus.energy
            general.ground = updatedStatus.ground
            general.water = updatedStatus.water
            general.moisture = updatedStatus.moisture
            general.latestUpdate = NSDate()
            general.cropCount = updatedStatus.cropCount
        }
    }
    
    // Deletes a Status
    func deleteStatus(id: NSManagedObjectID){
        if let statusToDelete = getById(id){
            context.deleteObject(statusToDelete)
        }
    }
}