//
//  File.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 7. 4..
//  Copyright © 2016년 Team6. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class AchievementService {
    let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    let formatter = NSNumberFormatter()
    
    func initData(){
        let data = getAll()
        if data.count < 1 {
            let list = AchievementList()
            let array = list.getArray()
            for no in 0 ..< array.count {
                var achieved:NSNumber = 0
                if no == 0 {
                    achieved = 2
                }
                let dictionary:NSMutableDictionary = array[no]
                let target:NSNumber = formatter.numberFromString(dictionary.valueForKey("target") as! String)!
                let prev:NSNumber = formatter.numberFromString(dictionary.valueForKey("prev") as! String)!
                let gold:NSNumber = formatter.numberFromString(dictionary.valueForKey("gold") as! String)!
                let energy:NSNumber = formatter.numberFromString(dictionary.valueForKey("energy") as! String)!
                let title:String = dictionary.valueForKey("title") as! String
                let body:String = dictionary.valueForKey("body") as! String
                let items:String = dictionary.valueForKey("items") as! String
                let criteria:String = dictionary.valueForKey("criteria") as! String
                
                create(no, title: title, body: body, prev: prev, gold: gold, items: items, energy: energy, achieved: achieved, target: target, criteria: criteria)
            }
        }
    }
    
    func create(no: NSNumber, title: String, body:String, prev:NSNumber, gold:NSNumber, items:String, energy:NSNumber, achieved:NSNumber, target:NSNumber, criteria:String) -> Achievement {
        
        let newItem = NSEntityDescription.insertNewObjectForEntityForName(Achievement.entityName, inManagedObjectContext: context) as! Achievement
        
        
        newItem.no = no
        newItem.title = title
        newItem.body = body
        newItem.prev = prev
        newItem.rewardGold = gold
        newItem.rewardItems = items
        newItem.rewardEnergy = energy
        newItem.achieved = achieved
        newItem.current = 0
        newItem.target = target
        newItem.criteria = criteria
        
        return newItem
    }
    
    func get(no:NSNumber) -> Achievement {
        let achievements:[Achievement] = getAll()
        
        if(achievements.count > 0){
            for achievement in achievements {
                if ( achievement.no == no ){
                    return achievement
                }
            }
            return Achievement()
        } else {
            return Achievement()
        }
    }
//    func get(no:NSNumber) -> Achievement {
//        
//        let predicate = NSPredicate(format: "no = %@", no)
//        let fetchRequest = NSFetchRequest(entityName: Achievement.entityName)
//        
//        fetchRequest.predicate = predicate
//        // var index = 0;
//        do {
//            let response = try context.executeFetchRequest(fetchRequest)
//            if(response.count < 1){
//                return Achievement()
//            } else {
//                return response.last as! Achievement
//            }
//        } catch let error as NSError {
//            // failure
//            print(error)
//            return Achievement()
//        }
//        
//    }
    // Gets a Status by id
    func getById(id: NSManagedObjectID) -> Achievement? {
        return context.objectWithID(id) as? Achievement
    }
    
    // Updates a Status
    func update(updatedStatus: Achievement){
        if let Achievement = getById(updatedStatus.objectID){
            Achievement.achieved = updatedStatus.achieved
        }
    }
    
    func getAll() -> [Achievement]{
        return getWith(withPredicate: NSPredicate(value:true))
    }
    
    func getWith(withPredicate queryPredicate: NSPredicate) -> [Achievement]{
        let fetchRequest = NSFetchRequest(entityName: Achievement.entityName)
        
        fetchRequest.predicate = queryPredicate
        
        do {
            let response = try context.executeFetchRequest(fetchRequest)
            return response as! [Achievement]
            
        } catch let error as NSError {
            // failure
            print(error)
            return [Achievement]()
        }
    }
    
    
    // Deletes a Status
    func deleteStatus(id: NSManagedObjectID){
        if let statusToDelete = getById(id){
            context.deleteObject(statusToDelete)
        }
    }
}
