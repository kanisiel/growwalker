//
//  AchievementList.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 7. 4..
//  Copyright © 2016년 Team6. All rights reserved.
//
//  Require Attributes :
//  @NSManaged var no: NSNumber?
//  @NSManaged var title: String?
//  @NSManaged var body: String?
//  @NSManaged var rewardGold: NSNumber?
//  @NSManaged var rewardEnergy: NSNumber?
//  @NSManaged var rewardItems: String?
//  @NSManaged var achieved: NSNumber?
//

import Foundation


class AchievementList {
    let formatter = NSNumberFormatter()
    var achievementDict = [NSMutableDictionary]()
    var dictionary = [String: String]()
    var itemArray:[[String: String]] = [
        ["body":"","title":"","prev":"0","gold":"","items":"","energy":"","current":"0","target":"0","criteria":""],
        ["body":"처음 식물을 심었다","title":"첫걸음","prev":"0","gold":"1000","items":"","energy":"","current":"0","target":"1","criteria":"plants"],
        ["body":"처음 식물을 수확했다","title":"첫 수확","prev":"0","gold":"1000","items":"","energy":"100","current":"0","target":"1","criteria":"plants"]
    ]
    func pibonacci(count:Int) -> Int {
        var array:[Int] = Array.init(count: count+3, repeatedValue: 1)
        if(count>0){
            for index in 1 ..< count {
                let buffer:Int = array[index - 1] + array[index]
                array[index+1] = buffer
            }
        }
        return array[count]
    }
    func makeDictionary(body:String, title:String, prev:Int, gold:Int, items:String, energy:Int, current:Int, target:Int, criteria:String) -> NSMutableDictionary {
        let formatter = NSNumberFormatter()
        let achieve:NSMutableDictionary = NSMutableDictionary.init(capacity: 9)
        achieve["body"] = body
        achieve["title"] = title
        achieve["prev"] = formatter.stringFromNumber(prev)
        achieve["target"] = formatter.stringFromNumber(target)
        achieve["criteria"] = criteria
        achieve["gold"] = formatter.stringFromNumber(gold)
        achieve["items"] = items
        achieve["energy"] = formatter.stringFromNumber(energy)
        achieve["current"] = "0"
        return achieve
    }
    func makeDataDict(capacity:Int) -> NSMutableDictionary {
        return NSMutableDictionary.init(capacity: capacity)
    }
    func addCollectAchieve(){
        let bodyAppend: String = " 종의 식물을 수집해보세요."
        let titlePrepend: String = "나는야 오박사 "
        
        let bodyAppend1: String = " 식물을 모두 수집해보세요."
        let titleAppend1: String = " 식물 마스터"
        
        let criteria: [Int] = [10, 25, 50, 100, 155]
        let rating: [String] = ["N", "UC", "R", "L"]
        let amount: [Int] = [80, 50, 20, 5]
        let ratingKor: [String] = ["흔한", "드문", "희귀한", "전설의"]
        var criteria1:[NSMutableDictionary] = []
        
        var size = achievementDict.count
        for index in 0 ..< rating.count {
            let dictionary:NSMutableDictionary = makeDataDict(3)
            dictionary["rating"] = rating[index]
            dictionary["ratingKor"] = ratingKor[index]
            dictionary["amount"] = amount[index]
            criteria1.append(dictionary)
        }
        for index in 0 ..< criteria.count {
            let body = formatter.stringFromNumber(criteria[index])!+bodyAppend
            let title = titlePrepend+"("+formatter.stringFromNumber(index+1)!+")"
            var prev = 0
            if index > 0 {
                prev = size-1
            }
            achievementDict.append(makeDictionary(body, title: title, prev: prev, gold: 0, items: "", energy: 0, current: 0, target: criteria[index], criteria: "collect"))
            size = achievementDict.count
        }
        for index in 0 ..< rating.count {
            let dictionary = criteria1[index]
            let body = dictionary.valueForKey("ratingKor") as! String+bodyAppend1
            let title = dictionary.valueForKey("ratingKor") as! String+titleAppend1
            var prev = 0
            if index == 2 {
                achievementDict.append(makeDictionary("희귀한 식물 10종을 수집해보세요.", title: "희귀한 식물 수집가", prev: 0, gold: 0, items: "", energy: 0, current: 0, target: 10, criteria: "collectRating"))
                prev = achievementDict.count-1
            } else if index == 3 {
//                achievementDict.append(makeDictionary("희귀한 식물 10종을 수집해보세요.", title: "희귀한 식물 수집가", prev: 0, gold: 0, items: "", energy: 0, current: 0, target: 10, criteria: "collectRating"))
            }
            achievementDict.append(makeDictionary(body, title: title, prev: prev, gold: 0, items: "", energy: 0, current: 0, target: dictionary.valueForKey("amount") as! Int, criteria: "collectRating"))
        }
    }
    func addCropAchieve() {
        var size = achievementDict.count
        let bodyAppend: String = " 번 수확해보세요."
        let titlePrepend: String = "수확의 참맛 "
        for index in 0 ..< 35 {
            var count = pibonacci(index)
            var prev = 0
            if(index > 0){
                count = count * 10
                prev = size-1
            }
            let body = formatter.stringFromNumber(count)!+bodyAppend
            let title = titlePrepend+"("+formatter.stringFromNumber(index+1)!+")"
             achievementDict.append(makeDictionary(body, title: title, prev: prev, gold: 0, items: "", energy: 0, current: 0, target: count, criteria: "crop"))
            size = achievementDict.count
        }
    }
    func addWalkAchieve() {
        var size = achievementDict.count
        let criteria: [Int] = [100,
            10000,
            50000,
            100000,
            250000,
            500000,
            750000,
            1000000,
            2500000,
            5000000,
            7500000,
            10000000,
            25000000,
            50000000,
            75000000,
            100000000]
        let formatter = NSNumberFormatter()
        
        let bodyAppend: String = " 걸음을 걸어보세요."
        let titlePrepend: String = "보람찬 노동 "
        for index in 0 ..< criteria.count {
            var prev = 0
            if index > 0 {
                prev = size-1
            }
            let body = formatter.stringFromNumber(criteria[index])!+bodyAppend
            let title = titlePrepend+"("+formatter.stringFromNumber(index+1)!+")"
            achievementDict.append(makeDictionary(body, title: title, prev: prev, gold: 0, items: "", energy: 0, current: 0, target: criteria[index], criteria: "step"))
            size = achievementDict.count
        }
        
    }
    func getArray() -> [NSMutableDictionary] {
        achievementDict.append(makeDictionary("", title: "", prev: 0, gold: 0, items: "", energy: 0, current: 0, target: 0, criteria: "base"))
        addWalkAchieve()
        addCropAchieve()
        addCollectAchieve()
        return achievementDict
    }
    
}