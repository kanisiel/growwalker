//
//  QuestList.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 7. 4..
//  Copyright © 2016년 Team6. All rights reserved.
//
//  Require Attributes :
//  @NSManaged var no: NSNumber?
//  @NSManaged var body: String?
//  @NSManaged var title: String?
//  @NSManaged var prev: NSNumber?
//  @NSManaged var rewardGold: NSNumber?
//  @NSManaged var rewardItems: String?
//  @NSManaged var rewardEnergy: NSNumber?
//  @NSManaged var clear: NSNumber?
//  @NSManaged var current: NSNumber?
//  @NSManaged var target: NSNumber?
//  @NSManaged var criteria: String?
//

import Foundation


class QuestList {
    var plantService:PlantsService?
    let formatter = NSNumberFormatter()
    var questDict = [NSMutableDictionary]()
    var dictionary = [String: String]()
    var itemArray:[[String: String]] = [
    ["body":"","title":"","prev":"0","gold":"0","items":"","energy":"0","current":"0","target":"0","criteria":""],
        ["body":"식물을 10개 수집하세요","title":"수집(1)","prev":"0","gold":"1000","items":"","energy":"0","current":"0","target":"10","criteria":"collection"],
        ["body":"식물을 1개 수확하세요","title":"수확(1)","prev":"0","gold":"1000","items":"","energy":"0","current":"0","target":"1","criteria":"crop"],
        ["body":"식물을 10개 수확하세요","title":"수확(2)","prev":"0","gold":"1000","items":"","energy":"0","current":"0","target":"10","criteria":"crop"],
        ["body":"식물을 20개 수확하세요","title":"수확(3)","prev":"0","gold":"1000","items":"","energy":"0","current":"0","target":"20","criteria":"crop"],
        ["body":"식물을 30개 수확하세요","title":"수확(4)","prev":"0","gold":"1000","items":"","energy":"0","current":"0","target":"30","criteria":"crop"]
    ]
    func setService(plantService:PlantsService){
        self.plantService = plantService
    }
    
    func makeDictionary(body:String, title:String, prev:Int, gold:Int, items:String, energy:Int, current:Int, target:Int, criteria:String) -> NSMutableDictionary {
        let formatter = NSNumberFormatter()
        let achieve:NSMutableDictionary = NSMutableDictionary.init(capacity: 9)
        achieve["body"] = body
        achieve["title"] = title
        achieve["prev"] = formatter.stringFromNumber(prev)
        achieve["target"] = formatter.stringFromNumber(target)
        achieve["criteria"] = criteria
        achieve["gold"] = formatter.stringFromNumber(gold)
        achieve["items"] = items
        achieve["energy"] = formatter.stringFromNumber(energy)
        achieve["current"] = "0"
        return achieve
    }
    func makeDataDict(capacity:Int) -> NSMutableDictionary {
        return NSMutableDictionary.init(capacity: capacity)
    }
    
    func makeDailyQuest(){
        let criteria:String = "daily"
        let cropAmount:Int = 20
        let cropAmountS = formatter.stringFromNumber(cropAmount)!
        let energyAmount:Int = 200000
        let energyAmountS = formatter.stringFromNumber(energyAmount)!
        let spentAmount:Int = 100000
        let spentAmountS = formatter.stringFromNumber(spentAmount)!
        questDict.append(makeDictionary("식물을 "+cropAmountS+"회 수확하세요", title: "식물 수확하기", prev: 0, gold: 1000, items: "", energy: 5000, current: 0, target: cropAmount, criteria: criteria))
        questDict.append(makeDictionary("운동으로 "+energyAmountS+"에너지를 획득하세요", title: "에너지 획득하기", prev: 0, gold: 10000, items: "", energy: 0, current: 0, target: energyAmount, criteria: criteria))
        questDict.append(makeDictionary(spentAmountS+"에너지를 사용하세요", title: "에너지 사용하기", prev: 0, gold: 3000, items: "", energy: 30000, current: 0, target: spentAmount, criteria: criteria))
        questDict.append(makeDictionary("", title: "", prev: 0, gold: 0, items: "", energy: 0, current: 0, target: 0, criteria: criteria+"spare"))
        questDict.append(makeDictionary("", title: "", prev: 0, gold: 0, items: "", energy: 0, current: 0, target: 0, criteria: criteria+"spare"))
    }
    func contains(value:Int, array:[Int]) -> BooleanType {
        var rv = false
        
        for v in array {
            if v == value {
                rv = true
                return rv
            }
        }
        
        return rv
    }
    func generateRandomNo(max:Int) -> Int {
        return Int(arc4random_uniform(UInt32(max)))
    }
    func makeCropQuests(plantsService:PlantsService){
        var size = questDict.count
        let questAmount:Int = 20
        let questAmount2:Int = 10
        var questArray: [Int] = Array.init(count: questAmount*2 + questAmount2, repeatedValue: -1)
        for index in 0..<questAmount {
            let amount:Int = plantsService.amounts["N"] as! Int
            var questNo:Int = -1
            repeat {
                questNo = Int(arc4random_uniform(UInt32(amount)))
            } while contains(questNo, array: questArray)
            questArray.append(questNo)
            let name:String = plantsService.get(questNo).name!
            var prev = 0
            if index > 0 {
                prev = size - 1
            }
            makeDictionary(name+"를 수확하세요", title: name+"수확하기", prev: prev, gold: 4000, items: "", energy: 10000, current: 0, target: 1, criteria: "cropQuest")
            size = questDict.count
        }
        for index in 0..<questAmount {
            let amount:Int = plantsService.amounts["UC"] as! Int
            var questNo:Int = -1
            repeat {
                questNo = Int(arc4random_uniform(UInt32(amount)))
            } while contains(questNo, array: questArray)
            questArray.append(questNo)
            let name:String = plantsService.get(questNo).name!
            let prev = size - 1

            makeDictionary(name+"를 수확하세요", title: name+"수확하기", prev: prev, gold: 4000, items: "", energy: 20000, current: 0, target: 1, criteria: "cropQuest")
            size = questDict.count
        }
        for _ in 0..<questAmount2 {
            let amount:Int = plantsService.amounts["R"] as! Int
            var questNo:Int = -1
            repeat {
                questNo = Int(arc4random_uniform(UInt32(amount)))
            } while contains(questNo, array: questArray)
            questArray.append(questNo)
            let name:String = plantsService.get(questNo).name!
            let prev = size - 1

            makeDictionary(name+"를 수확하세요", title: name+"수확하기", prev: prev, gold: 4000, items: "", energy: 50000, current: 0, target: 1, criteria: "cropQuest")
            size = questDict.count
        }
    }
    func makeExercizeQuest(){
        
    }
    func getDictionary() -> NSDictionary {
        var dict: [Int: NSDictionary] = [:]
        for no in 0 ..< itemArray.count{
            let noString = formatter.stringFromNumber(no)
            itemArray[no]["no"] = noString
            dict[no] = NSDictionary.init(dictionary: itemArray[no])
        }
        return NSDictionary.init(dictionary: dict)
    }
    
    func getArray(plantsService:PlantsService) -> [NSMutableDictionary] {
        questDict.append(makeDictionary("", title: "", prev: 0, gold: 0, items: "", energy: 0, current: 0, target: 0, criteria: "base"))
        makeDailyQuest()
        makeCropQuests(plantsService)
        return questDict
    }
    
}