//
//  ItemsList.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 7. 4..
//  Copyright © 2016년 Team6. All rights reserved.
//
//  Requred Attributes
//  @NSManaged var no: NSNumber?
//  @NSManaged var name: String?
//  @NSManaged var desc: String?
//  @NSManaged var cost: NSNumber?

import Foundation


class ItemsList {
    
    var itemsDict = [Int: Array<String>]()
    var itemArray:[[String: String]] = [
        ["name":"영양제", "desc":"식물이 빠르고 건강하게 자랄 수 있도록 도와주는 고급 영양제.","cost":"5000"],
        ["name":"살수기", "desc":"땅이 마르지 않도록 자동으로 물을 뿌려주는 기계. 물탱크가 비지 않도록 채워줘야 한다.","cost":"3000000"],
        ["name":"만년삼", "desc":"만년묵은 산삼. 매우 영험할것 같다.","cost":"-999"],
        ["name":"마리오버섯", "desc":"사람도 작물도 매우 크게 자랄것 같은 버섯이다.","cost":"-999"],
        ["name":"만드라고라", "desc":"뽑으면 비명을 지르는 신기한 식물. 꽤 영험해 보인다.","cost":"-999"],
        ["name":"선두", "desc":"먹으면 최상의 상태가 되게 해주는 신비한 콩.","cost":"-999"],
        ["name":"천도복숭아", "desc":"옥황상제도 자주 먹지 못한다는 매우 귀한 천상의 복숭아. 손오공이 훔쳐 먹었다가 큰 벌을 받았다.","cost":"-999"]
    ]

    func getArray() -> [[String: String]] {
        return itemArray
    }
    
}