import UIKit

class CardView: UIViewController {
    
    @IBOutlet weak var Plant: UIImageView!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var Lv: UILabel!
    @IBOutlet weak var Information: UILabel!
    
    
    func setItem(item:GalleryItem) {
        Plant.image = UIImage(named: item.itemImage)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}