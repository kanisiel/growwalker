//
//  HelthManager.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 6. 27..
//  Copyright © 2016년 Team6. All rights reserved.
//

import Foundation
import HealthKit

class HealthKitManager {
    
    let timeService:TimeService = TimeService()
    let healthKitStore: HKHealthStore = HKHealthStore()
    
    func authorizeHealthKit(completion: ((success: Bool, error: NSError!) -> Void)!) {
        
        // State the health data type(s) we want to read from HealthKit.
        let healthDataToRead = Set(arrayLiteral: HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeight)!, HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)!,HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning)!,HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)!)
        
        // State the health data type(s) we want to write from HealthKit.
        let healthDataToWrite = Set(arrayLiteral: HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning)!, HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)!)
        
        // Just in case OneHourWalker makes its way to an iPad...
        if !HKHealthStore.isHealthDataAvailable() {
            print("Can't access HealthKit.")
        }
        
        // Request authorization to read and/or write the specific data.
        healthKitStore.requestAuthorizationToShareTypes(healthDataToWrite, readTypes: healthDataToRead) { (success, error) -> Void in
            if( completion != nil ) {
                completion(success:success, error:error)
            }
        }
    }
    func queryDailyStats(metric: NSString!, handler: ([NSDate: Int] -> Void)!) {
        let quantityType = HKObjectType.quantityTypeForIdentifier(metric as String)
        let calendar = NSCalendar.currentCalendar()
        let now = NSDate()
        let preservedComponents: NSCalendarUnit = [.Year , .Month , .Day]
        let midnight: NSDate! = calendar.dateFromComponents(calendar.components(preservedComponents, fromDate:now))
        let dailyInterval = NSDateComponents()
        dailyInterval.day = 1
        
        let tomorrow = calendar.dateByAddingUnit(.Month, value: 1, toDate: midnight, options: [])
        let oneMonthAgo = calendar.dateByAddingUnit(.Month, value: -1, toDate: midnight, options: [])
        let oneWeekAgo = calendar.dateByAddingUnit(.Day, value: -6, toDate: midnight, options: []) // only need to start from 6 days back
        
        // need tomorrow's midnight as end date to get all of today's data
        let predicate = HKQuery.predicateForSamplesWithStartDate(oneWeekAgo, endDate: tomorrow, options: .None)
        
        let query = HKStatisticsCollectionQuery(
            quantityType: quantityType!,
            quantitySamplePredicate: predicate,
            options: .CumulativeSum,
            anchorDate: midnight,
            intervalComponents: dailyInterval
        )
        
        query.initialResultsHandler = { query, results, error -> Void in
            var data:[NSDate: Int] = [:]
            if error != nil {
                print(error)
            } else {
                results!.enumerateStatisticsFromDate(oneWeekAgo!, toDate: tomorrow!) { statistics, stop in
                    if let quantity = statistics.sumQuantity() {
                        let date = statistics.startDate
                        let value = Int(quantity.doubleValueForUnit(HKUnit.countUnit()))
                        
                        data[date] = value
                    }
                }
            }
            
            handler(data)
        }
        
        self.healthKitStore.executeQuery(query)
    }
    func recentSteps(completion: ((HKSample!, NSError!) -> Void)!)
    {
        let locale:NSLocale = NSLocale.currentLocale()
        // The type of data we are requesting (this is redundant and could probably be an enumeration
        let type = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)!
        
        let calendar = NSCalendar.currentCalendar()
        calendar.locale = locale
        
        let year = 1970
        let month = 1
        let day = 1
        
        let yesterday = makeDate(year, month: month, day: day)
        
        
        // Our search predicate which will fetch data from now until a day ago
        // (Note, 1.day comes from an extension
        // You'll want to change that to your own NSDate
        let predicate = HKQuery.predicateForSamplesWithStartDate (yesterday, endDate: NSDate(), options: .None)
        
        // The actual HealthKit Query which will fetch all of the steps and sub them up for us.
        let query = HKSampleQuery(sampleType: type, predicate: predicate, limit: 0, sortDescriptors: nil) { query, results, error in
            if let queryError = error {
                completion(nil, queryError)
                return
            }
            
            // Set the first HKQuantitySample in results as the most recent height.
            let lastSteps = results!.last!
            
            if completion != nil {
                completion(lastSteps, nil)
            }
        }
        
        self.healthKitStore.executeQuery(query)
    }
    func makeDate(year:Int, month:Int, day:Int) -> NSDate {
        let calendar = NSCalendar.currentCalendar()
        let dateComponents = NSDateComponents()
        
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        dateComponents.hour = 0
        dateComponents.minute = 0
        dateComponents.second = 0
        dateComponents.nanosecond = 0
        let date = calendar.dateFromComponents(dateComponents)
        return date!
    }
    func makeDateTime(year:Int, month:Int, day:Int, hour:Int, minute:Int, second:Int) -> NSDate {
        let calendar = NSCalendar.currentCalendar()
        let dateComponents = NSDateComponents()
        
        dateComponents.year = year
        dateComponents.month = month
        dateComponents.day = day
        dateComponents.hour = hour
        dateComponents.minute = minute
        dateComponents.second = second
        dateComponents.nanosecond = 0
        
        let datetime = calendar.dateFromComponents(dateComponents)
        return datetime!
    }
    func getSteps(startDate:NSDate, sampleType: HKSampleType , completion: ((HKQuantity!, NSError!) -> Void)!) {
        // Predicate for the height query
        let currentDate = NSDate()
        
        let yesterday = startDate
        
        
        let lastStepPredicate = HKQuery.predicateForSamplesWithStartDate(yesterday, endDate: currentDate, options: .None)
        
        // Get the single most recent height
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        
        // Query HealthKit for the last Height entry.
        let stepQuery = HKSampleQuery(sampleType: sampleType, predicate: lastStepPredicate, limit: 15000, sortDescriptors: [sortDescriptor]) { (sampleQuery, results, error ) -> Void in
            
            if let queryError = error {
                completion(nil, queryError)
                return
            }
            let quantity:HKQuantity
            var steps: Double = 0
            
            if results?.count > 0
            {
                for result in results as! [HKQuantitySample]
                {
                    steps += result.quantity.doubleValueForUnit(HKUnit.countUnit())
                }
                quantity = HKQuantity.init(unit: HKUnit.countUnit(), doubleValue: steps)
                
                let dayStep = quantity
                if completion != nil {
                    completion(dayStep, nil)
                }

            } else {
                quantity = HKQuantity.init(unit: HKUnit.countUnit(), doubleValue: 0)
                completion(quantity, error)
                return
            }
            // Set the first HKQuantitySample in results as the most recent height.
        }
        
        // Time to execute the query.
        self.healthKitStore.executeQuery(stepQuery)
    }
    func getHeight(sampleType: HKSampleType , completion: ((HKSample!, NSError!) -> Void)!) {
        
        // Predicate for the height query
        let distantPastHeight = NSDate.distantPast() as NSDate
        let currentDate = NSDate()
        let lastHeightPredicate = HKQuery.predicateForSamplesWithStartDate(distantPastHeight, endDate: currentDate, options: .None)
        
        // Get the single most recent height
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        
        // Query HealthKit for the last Height entry.
        let heightQuery = HKSampleQuery(sampleType: sampleType, predicate: lastHeightPredicate, limit: 1, sortDescriptors: [sortDescriptor]) { (sampleQuery, results, error ) -> Void in
            
            if let queryError = error {
                completion(nil, queryError)
                return
            }
            
            // Set the first HKQuantitySample in results as the most recent height.
            let lastHeight = results!.first
            
            if completion != nil {
                completion(lastHeight, nil)
            }
        }
        
        // Time to execute the query.
        self.healthKitStore.executeQuery(heightQuery)
    }
    
    func getWeight(completion: ((Double!, NSError!) -> Void)!) {
        let weightSampleType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
        // Predicate for the height query
        let distantPastWeight = NSDate.distantPast() as NSDate
        let currentDate = NSDate()
        let lastWeightPredicate = HKQuery.predicateForSamplesWithStartDate(distantPastWeight, endDate: currentDate, options: .None)
        
        // Get the single most recent height
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        
        // Query HealthKit for the last Height entry.
        let weightQuery = HKSampleQuery(sampleType: weightSampleType!, predicate: lastWeightPredicate, limit: 1, sortDescriptors: [sortDescriptor]) { (sampleQuery, results, error ) -> Void in
            
            if let queryError = error {
                completion(nil, queryError)
                return
            }
            if(results?.count > 0){
                // Set the first HKQuantitySample in results as the most recent height.
                let lastWeight = results!.first as! HKQuantitySample
                if completion != nil {
                    completion(lastWeight.quantity.doubleValueForUnit(HKUnit.gramUnit()), nil)
                }
            } else {
//                // Set the quantity type to the weight.
//                let weightType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
//                // Set the unit of measurement to gram.
//                let weightQuantity = HKQuantity(unit: HKUnit.gramUnit(), doubleValue: 65000)
//                // Set the official Quantity Sample.
//                let weight = HKQuantitySample(type: weightType!, quantity: weightQuantity, startDate: NSDate(), endDate: NSDate())
                if completion != nil {
                    completion(65000, nil)
                }
            }
        }
        
        // Time to execute the query.
        self.healthKitStore.executeQuery(weightQuery)
    }
    
    func saveWeight(inputedWeight: Double, date: NSDate ) {
        let formatter = NSNumberFormatter()
        // Set the quantity type to the weight.
        let weightType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMass)
        
        // Set the unit of measurement to gram.
        let weightQuantity = HKQuantity(unit: HKUnit.gramUnit(), doubleValue: inputedWeight)
        
        // Set the official Quantity Sample.
        let weight = HKQuantitySample(type: weightType!, quantity: weightQuantity, startDate: date, endDate: date)
        
        // Save the bodymass quantity sample to the HealthKit Store.
        healthKitStore.saveObject(weight, withCompletion: { (success, error) -> Void in
            if( error != nil ) {
                print(error)
            } else {
                print("weight saved.")
                print("--- Weights ---")
                print("----- End -----")
                let kg = weight.quantity.doubleValueForUnit(HKUnit.gramUnit())/1000
                var kgString = "   "
                kgString.appendContentsOf(formatter.stringFromNumber(kg)!)
                print(kgString.appendContentsOf(" kg   "))
                print("----- End -----")
            }
        })
    }
    
    func saveDistance(distanceRecorded: Double, date: NSDate ) {
        
        // Set the quantity type to the running/walking distance.
        let distanceType = HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning)
        
        // Set the unit of measurement to miles.
        let distanceQuantity = HKQuantity(unit: HKUnit.mileUnit(), doubleValue: distanceRecorded)
        
        // Set the official Quantity Sample.
        let distance = HKQuantitySample(type: distanceType!, quantity: distanceQuantity, startDate: date, endDate: date)
        
        // Save the distance quantity sample to the HealthKit Store.
        healthKitStore.saveObject(distance, withCompletion: { (success, error) -> Void in
            if( error != nil ) {
                print(error)
            } else {
                print("The distance has been recorded! Better go check!")
            }
        })
    }
    
    func getDistance(fromDate:NSDate, completion: (([HKQuantitySample!], NSError!) -> Void)!) {
        // Set the Sample type to the running/walking distance.
        let distanceSampleType = HKSampleType.quantityTypeForIdentifier(HKQuantityTypeIdentifierDistanceWalkingRunning)!
        let currentDate = NSDate()
        
        
        let lastDistancePredicate = HKQuery.predicateForSamplesWithStartDate(fromDate, endDate: currentDate, options: .None)
        
        // Get the single most recent height
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        
        // Query HealthKit for the last Height entry.
        let distanceQuery = HKSampleQuery(sampleType: distanceSampleType, predicate: lastDistancePredicate, limit: 15000, sortDescriptors: [sortDescriptor]) { (sampleQuery, results, error ) -> Void in
            
            if let queryError = error {
                completion([], queryError)
                return
            }
            
            if results?.count > 0
            {
                if completion != nil {
                    completion(results as! [HKQuantitySample], nil)
                }
                
            } else {
                completion([], error)
                return
            }
            // Set the first HKQuantitySample in results as the most recent height.
        }
        
        // Time to execute the query.
        self.healthKitStore.executeQuery(distanceQuery)
    }
    
    func getMET(velocity:Double) -> Double {
        var rv:Double = 0.9
        let vArray:[Double] = [2.6, 2.7, 4, 4.8, 5.5, 6, 8, 9.7]
        let mArray:[Double] = [2, 2.3, 2.9, 3.3, 3.6, 7, 8, 10]

        if(velocity <= vArray[0]){
            rv = mArray[0]
        } else if (vArray[0] < velocity && velocity <= vArray[1]){
            rv = mArray[1]
        } else if (vArray[1] < velocity && velocity <= vArray[2]){
            rv = mArray[2]
        } else if (vArray[2] < velocity && velocity <= vArray[3]){
            rv = mArray[3]
        } else if (vArray[3] < velocity && velocity <= vArray[4]){
            rv = mArray[4]
        } else if (vArray[4] < velocity && velocity <= vArray[5]){
            rv = mArray[5]
        } else if (vArray[5] < velocity && velocity <= vArray[6]){
            rv = mArray[6]
        } else if (vArray[6] < velocity){
            rv = mArray[7]
        }
        
        return rv
    }
    func getVelocity(action:HKQuantitySample) -> Double {
        let comp:NSDateComponents = timeService.getTimeDiff(action.startDate, toDate: action.endDate)
        let seconds:Int = timeService.getSeconds(comp)
        let distance = action.quantity.doubleValueForUnit(HKUnit.meterUnit())
        let velocity:Double = (distance/Double(seconds))*(3600/1000)
        
        return velocity
    }
    func getConsumedOxygen(mass:Double, action:HKQuantitySample) -> Double {
        let comp:NSDateComponents = timeService.getTimeDiff(action.startDate, toDate: action.endDate)
        let seconds:Int = timeService.getSeconds(comp)
        let minute:Int = seconds/60
        let oxygenPerKG:Double = 3.5
        let velocity:Double = getVelocity(action)
        let met:Double = getMET(velocity)
        return met * oxygenPerKG * mass * Double(minute)
    }
    
    func getConsumedCalorie(mass:Double, action:HKQuantitySample) -> Double {
        let consumedOxygen:Double = getConsumedOxygen(mass, action: action)
        
        return consumedOxygen * 5000
    }
    // To Do
    //체중 기본값 65kg
    // 체중 get, save
    // 거리 get
}